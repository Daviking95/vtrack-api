const winston = require('winston');
const mongoose = require('mongoose');
const config = require('config');
require('dotenv').config();

module.exports = function () {

  let connectionUrl;
  // Dont forget to change to production once done.
  if (process.env.NODE_ENV === 'production') connectionUrl = process.env.MONGO_PROD_LIVE_URL;
  else if (process.env.NODE_ENV === 'production-staging') connectionUrl = process.env.MONGO_STAGING_URL;
  else if (process.env.NODE_ENV === 'local-development') connectionUrl = process.env.MONGO_LOCAL_LIVE_URL;
  else connectionUrl = process.env.MONGO_STAGING_URL;

  mongoose.set('useCreateIndex', true)
  mongoose.connect(connectionUrl, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false
    })
    .then(() => winston.info('Connected to MongoDB...'));
}