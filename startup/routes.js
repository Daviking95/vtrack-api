const express = require('express');
const user = require('../routes/users');
const admin = require('../routes/admin');
const monitors = require('../routes/monitors');
const forms = require('../routes/forms');
const adminLogs = require('../routes/adminLogs');
// const localGovt = require('../routes/localGovt');
// const movie = require('../routes/movie');
// const rating = require('../routes/rating');
// const actors = require('../routes/actors');
// const reviews = require('../routes/review');
// const news = require('../routes/news');
// const comments = require('../routes/comments');
// const favourite = require('../routes/favourite');
// const advert = require('../routes/advert');
// const likes = require('../routes/likes');

const error = require('../middlewares/error');
const jsonError = require('../middlewares/jsonError');

module.exports = function(app) {
  app.use(express.json());
  app.use('/api', user);
  app.use('/api', admin);
  app.use('/api', monitors);
  app.use('/api', forms);
  app.use('/api', adminLogs);
  // app.use('/api', localGovt);
  // app.use('/api', movie);
  // app.use('/api', rating);
  // app.use('/api', actors);
  // app.use('/api', reviews);
  // app.use('/api', news);
  // app.use('/api', comments);
  // app.use('/api', favourite);
  // app.use('/api', advert);
  // app.use('/api', likes);

  app.use('/', (req, res) => res.status(404).send(jsonError(404, false, 'Welcome To VTrack!!!. You got here through a wrong route')));
  app.use('*', (req, res, next) => {
    res.status(404).json({
      error: 'Invalid route'
    });
    next();
  });
  app.use(error);
}
