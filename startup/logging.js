const winston = require('winston');
const config = require('config');
require('winston-mongodb');
require('express-async-errors');
require('dotenv').config();

let connectionUrl;
// Dont forget to change to production once done.
if (process.env.NODE_ENV === 'production') connectionUrl = process.env.MONGO_PROD_LIVE_URL;else if (process.env.NODE_ENV === 'production-staging') connectionUrl = process.env.MONGO_STAGING_URL;else if (process.env.NODE_ENV === 'local-development') connectionUrl = process.env.MONGO_LOCAL_LIVE_URL;else connectionUrl = process.env.MONGO_STAGING_URL;

module.exports = function() {
  winston.exceptions.handle(
    new winston.transports.File({ filename: 'uncaughtExceptions.log' })); 
  
  process.on('unhandledRejection', (ex) => { 
    throw ex;
  });
  
  winston.add(winston.transports.File, { filename: 'logfile.log' });
  winston.add(winston.transports.MongoDB, { 
    db: connectionUrl,
    level: 'info'
  });  

  process.on('SIGINT', async () => {
    await mongoose.connection.close(); // close DB
    winston.info('Shutting down server');
    winston.info('Server successfully shut down');
    process.exit(0);
  });
  
}