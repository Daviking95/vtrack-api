const _ = require('lodash');
const {
    Form
} = require('../models/formModel');
const {
    AdminLog
} = require('../models/adminLogsModel');
const {
    Admin
} = require('../models/adminModel');
const jsonError = require('../middlewares/jsonError');
var Fawn = require('fawn');
const mongoose = require('mongoose');

Fawn.init(mongoose);

var task = Fawn.Task();

function returnFormObjects(req) {

    const formObject = {
        monitorName: req.monitorName,
        monitorCode: req.monitorCode,
        dateOfReport: req.dateOfReport,
        formCode: req.formCode,
        electionType: req.electionType,
        dateOfIncidence: req.dateOfIncidence,
        timeOfIncidence: req.timeOfIncidence,
        geoPoliticalZone: req.geoPoliticalZone,
        city: req.city,
        state: req.state,
        incidentTitle: req.incidentTitle,
        incidentDesc: req.incidentDesc,
        incidentSource: req.incidentSource,
        mediaSource: req.mediaSource,
        incidentLocation: req.incidentLocation,
        violenceInitiatorPeopleAndGenderInvolved: req.violenceInitiatorPeopleAndGenderInvolved,
        violenceInitiator: req.violenceInitiator,
        violenceVictimPeopleAndGenderInvolved: req.violenceVictimPeopleAndGenderInvolved,
        incidentViolenceVictims: req.incidentViolenceVictims,
        incidentViolence: req.incidentViolence,
        incidentWeapon: req.incidentWeapon,
        incidentImpact: req.incidentImpact,
        afterIncident: req.afterIncident,
        moreReportDetails: req.moreReportDetails,
        mediaAsset: req.mediaAsset,
        timeOfReport: req.timeOfReport,
        locationOfReport: req.locationOfReport,
        formSubmissionPoint: req.formSubmissionPoint,
    };

    return formObject;
}

module.exports = {

    addForm: async (req, res) => {

        let form;

        if (req.body.formCode !== "" && req.body.formCode !== null) {
            form = await Form.findOne({
                formCode: req.body.formCode
            });
            
            if (form) return res.status(400).send(jsonError(400, false, 'Form exists.'));

        }

        form = new Form(returnFormObjects(req.body));
        form = await form.save();

        res.send(jsonError(200, true, "Form Added", form));
    },

    addMultipleForms: async (req, res) => {

        for (let index = 0; index < req.body.multiple.length; index++) {
            const element = req.body.multiple[index];

            let form = await Form.findOne({
                formCode: element.formCode
            });
            if (form) return res.status(400).send(jsonError(400, false, `Form code ${element.formCode} exists.`));

            form = new Form(returnFormObjects(element));
            form = await form.save();

        }

        res.send(jsonError(200, true, "Form List has been added successfully"));

    },

    getAllForms: async (req, res) => {
        const forms = await Form.find().sort('incidentTitle');
        res.send(jsonError(200, true, `Form List Generated with total of ${forms.length}`, forms));
    },

    getAwaitingVerificationForms: async (req, res) => {

        const forms = await Form.find({
            firstApproval: false
        }).sort('incidentTitle');
        res.send(jsonError(200, true, `Form List Generated with total of ${forms.length}`, forms));
    },

    getAwaitingApprovalForms: async (req, res) => {
        const forms = await Form.find({
            secondApproval: false
        }).sort('incidentTitle');
        res.send(jsonError(200, true, `Form List Generated with total of ${forms.length}`, forms));
    },

    // getFormByTypeFromForms: async (req, res) => {
    //     let searchForm = {
    //         [req.params.type]: req.params.value
    //     };
    //     const forms = await Form.find(searchForm).sort('incidentTitle');
    //     res.send(jsonError(200, true, forms.length, forms));
    // },

    getFormByTypeAndValueFromForms: async (req, res) => {
        let searchForm = {
            [req.params.type]: req.params.value
        };
        const forms = await Form.find(searchForm).sort('incidentTitle');
        res.send(jsonError(200, true, forms.length, forms));
    },

    getUserForms: async (req, res) => {
        const forms = await Form.find({
            monitorCode: req.user.user_monitor_code
        }).sort('incidentTitle');
        res.send(jsonError(200, true, `Form List Generated for ${forms.monitorName}`, forms));
    },

    getOneForm: async (req, res) => {
        const form = await Form.findOne({
            _id: req.params.id
        });

        if (!form) return res.status(404).send(jsonError(404, false, 'The form with the given title was not found.'));

        res.send(jsonError(200, true, 'Form found', form));
    },

    updateForm: async (req, res) => {

        let adminLogs;
        let admin = await Admin.findById(req.user._id);
        if (!admin) return res.status(404).send(jsonError(404, false, 'Admin not found.'));

        form = await Form.findByIdAndUpdate(req.params.id, {
            monitorName: req.body.monitorName,
            monitorCode: req.body.monitorCode,
            dateOfReport: req.body.dateOfReport,
            electionType: req.body.electionType,
            dateOfIncidence: req.body.dateOfIncidence,
            timeOfIncidence: req.body.timeOfIncidence,
            geoPoliticalZone: req.body.geoPoliticalZone,
            city: req.body.city,
            state: req.body.state,
            incidentTitle: req.body.incidentTitle,
            incidentDesc: req.body.incidentDesc,
            incidentSource: req.body.incidentSource,
            mediaSource: req.body.mediaSource,
            incidentLocation: req.body.incidentLocation,
            violenceInitiatorPeopleAndGenderInvolved: req.body.violenceInitiatorPeopleAndGenderInvolved,
            violenceInitiator: req.body.violenceInitiator,
            violenceVictimPeopleAndGenderInvolved: req.body.violenceVictimPeopleAndGenderInvolved,
            incidentViolenceVictims: req.body.incidentViolenceVictims,
            incidentViolence: req.body.incidentViolence,
            incidentWeapon: req.body.incidentWeapon,
            incidentImpact: req.body.incidentImpact,
            afterIncident: req.body.afterIncident,
            moreReportDetails: req.body.moreReportDetails,
            mediaAsset: req.body.mediaAsset,
            timeOfReport: req.body.timeOfReport,
            locationOfReport: req.body.locationOfReport,
            formSubmissionPoint: req.body.formSubmissionPoint,

        }, {
            new: true
        });

        if(form) {
            adminLogs = new AdminLog({
                email : admin.email,
                phoneNumber: admin.phoneNumber,
                formCode : form.formCode,
                incidentTitle : form.incidentTitle,
                incidentDesc: form.incidentDesc,
                activityType: "Update",
                activityDesc : `This admin updated this form`
            });
            
            await adminLogs.save();
        }

        if (!form) return res.status(404).send(jsonError(404, false, 'The form with the given ID was not found.'));

        res.send(jsonError(200, true, 'Form updated', form));
    },

    verifyForm: async (req, res) => {

        let adminLogs;
        let admin = await Admin.findById(req.user._id);
        if (!admin) return res.status(404).send(jsonError(404, false, 'Admin not found.'));

        let form = await Form.findOne({
            _id: req.params.id
        });
        if (!form) return res.status(404).send(jsonError(404, false, 'Form Code not found.'));

        let formApproval = !form.firstApproval;

        form = await Form.findByIdAndUpdate(form._id, {
            $set: {
                firstApproval: formApproval,
            }
        }, {
            new: true
        });

        // console.log(form);
        let verifyString = formApproval ? "verified" : "unverified";

        if(form) {
            adminLogs = new AdminLog({
                email : admin.email,
                phoneNumber: admin.phoneNumber,
                formCode : form.formCode,
                incidentTitle : form.incidentTitle,
                incidentDesc: form.incidentDesc,
                activityType: "Verification",
                activityDesc : `This admin ${verifyString} this form`
            });
            
            await adminLogs.save();
        }

        res.send(jsonError(200, true, `Form has been ${verifyString}`));
    },

    approveForm: async (req, res) => {

        let adminLogs;
        let admin = await Admin.findById(req.user._id);
        if (!admin) return res.status(404).send(jsonError(404, false, 'Admin not found.'));

        let form = await Form.findOne({
            _id: req.params.id
        });
        if (!form) return res.status(404).send(jsonError(404, false, 'Form Code not found.'));

        let formApproval = !form.secondApproval;

        form = await Form.findByIdAndUpdate(form._id, {
            $set: {
                secondApproval: formApproval,
            }
        }, {
            new: true
        });

        let approveString = formApproval ? "approved" : "unapproved";

        if(form) {
            adminLogs = new AdminLog({
                email : admin.email,
                phoneNumber: admin.phoneNumber,
                formCode : form.formCode,
                incidentTitle : form.incidentTitle,
                incidentDesc: form.incidentDesc,
                activityType: "Approval",
                activityDesc : `This admin ${approveString} this form`
            });
            
            await adminLogs.save();
        }

        res.send(jsonError(200, true, `Form has been ${approveString}`));
    },

    deleteOneForm: async (req, res) => {

        let adminLogs;
        let admin = await Admin.findById(req.user._id);
        if (!admin) return res.status(404).send(jsonError(404, false, 'Admin not found.'));

        const form = await Form.findByIdAndRemove(req.params.id);

        if(form) {
            adminLogs = new AdminLog({
                email : admin.email,
                phoneNumber: admin.phoneNumber,
                formCode : form.formCode,
                incidentTitle : form.incidentTitle,
                incidentDesc: form.incidentDesc,
                activityType: "Deletion",
                activityDesc : `This admin deleted this form`
            });
            
            await adminLogs.save();
        }

        if (!form) return res.status(404).send(jsonError(404, false, 'The form with the given ID was not found.'));

        res.send(jsonError(200, true, `Form has been deleted`));
    },

}