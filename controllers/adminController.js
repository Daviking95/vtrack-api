const bcrypt = require('bcryptjs');
// const argon2 = require('argon2');
const _ = require('lodash');
const {
  Admin
} = require('../models/adminModel');
const {
  AdminLog
} = require('../models/adminLogsModel');
const jsonError = require('../middlewares/jsonError');
const randomString = require('../utils/randomString');

module.exports = {

  registerAdmin: async (req, res) => {

    let admin = await Admin.findOne({
      email: req.body.email
    });
    if (admin) return res.status(400).send(jsonError(400, false, 'Email must be unique.'));

    if (await Admin.findOne({
        phoneNumber: req.body.phoneNumber
      })) return res.status(400).send(jsonError(400, false, 'Phone number must be unique.'));

    admin = new Admin(_.pick(req.body, ['firstName', 'lastName', 'email', 'phoneNumber', 'password', 'role']));

    admin.isAdmin = true;

    const salt = await bcrypt.genSalt(10);
    admin.password = await bcrypt.hash(admin.password, salt);
    // admin.password = await bcrypt.hash(admin.password);
    await admin.save();

    let responseMessage = "Admin Created Successfully";
    if (req.body.role === "verifier") responseMessage = "Verifier Created Successfully";

    const token = admin.generateAuthToken();

    adminLogs = new AdminLog({
      email: admin.email,
      phoneNumber: admin.phoneNumber,
      formCode: "",
      incidentTitle: "",
      incidentDesc: "",
      activityType: "Registration",
      activityDesc: `This admin was registered`
    });

    await adminLogs.save();

    res.header('x-auth-token', token).send(jsonError(200, true, responseMessage));
  },

  loginAdmin: async (req, res) => {

    let admin = await Admin.findOne({
      email: req.body.email
    });
    if (!admin) return res.status(400).send(jsonError(400, false, 'Invalid email or password.'));

    const validPassword = await bcrypt.compare(req.body.password, admin.password);
    if (!validPassword) return res.status(400).send(jsonError(400, false, 'Invalid email or password.'));

    let responseMessage = "Admin Login Successfully";
    if (admin.role === "verifier") responseMessage = "Verifier Login Successfully";

    const token = admin.generateAuthToken();

    adminLogs = new AdminLog({
      email: admin.email,
      phoneNumber: admin.phoneNumber,
      formCode: "",
      incidentTitle: "",
      incidentDesc: "",
      activityType: "Authentication",
      activityDesc: `This admin was logged in`
    });

    await adminLogs.save();

    res.status(200).send(jsonError(200, true, responseMessage, {
      token: token,
      role: admin.role
    }));
  },

  getCurrentAdmin: async (req, res) => {
    const admin = await Admin.findById(req.user._id).select('-password -isAdmin -__v');
    if (!admin) return res.status(404).send(jsonError(404, false, 'Admin not found.'));

    res.send(jsonError(200, true, 'Admin data gotten', admin));
  },

  updateAdminProfile: async (req, res) => {

    let admin = await Admin.findById(req.user._id);
    if (!admin) return res.status(404).send(jsonError(404, false, 'Invalid request.'));

    admin = await Admin.findByIdAndUpdate(req.user._id, {
      $set: {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        phoneNumber: req.body.phoneNumber,
        picture: req.body.picture,
      }
    }, {
      new: true
    });

    admin = await Admin.findById(req.user._id).select('-password -isAdmin -__v');

    adminLogs = new AdminLog({
      email: admin.email,
      phoneNumber: admin.phoneNumber,
      formCode: "",
      incidentTitle: "",
      incidentDesc: "",
      activityType: "Update",
      activityDesc: `This admin profile was updated`
    });

    await adminLogs.save();

    res.status(200).send(jsonError(200, true, 'Profile Updated', admin));
  },


  updateOneAdminProfile: async (req, res) => {

    let admin = await Admin.findById(req.params.id);
    if (!admin) return res.status(404).send(jsonError(404, false, 'Admin not found.'));

    // if(await Admin.findOne({
    //   email: req.body.email
    // })) return res.status(400).send(jsonError(400, false, 'Email must be unique.'));

    // if (await Admin.findOne({
    //   phoneNumber : req.body.phoneNumber
    // })) return res.status(400).send(jsonError(400, false, 'Phone number must be unique.'));

    admin = await Admin.findByIdAndUpdate(admin._id, {
      $set: {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        phoneNumber: req.body.phoneNumber,
        picture: req.body.picture,
        role: req.body.role
      }
    }, {
      new: true
    });

    admin = await Admin.findById(req.user._id).select('-password -isAdmin -__v');

    adminLogs = new AdminLog({
      email: admin.email,
      phoneNumber: admin.phoneNumber,
      formCode: "",
      incidentTitle: "",
      incidentDesc: "",
      activityType: "Update",
      activityDesc: `This admin profile was updated`
    });

    await adminLogs.save();

    res.status(200).send(jsonError(200, true, 'Profile Updated', admin));
  },


  forgotAdminPassword: async (req, res) => {

    let admin = await Admin.findOne({
      email: req.body.email
    });
    if (!admin) return res.status(404).send(jsonError(404, false, 'Admin not found.'));

    let passwordGenerated = randomString(20, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');

    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(passwordGenerated, salt);

    admin = await Admin.findByIdAndUpdate(admin._id, {
      $set: {
        password: hashedPassword,
      }
    }, {
      new: true
    });

    // todo: Use SendGrid API to send new password Generated to admin instead of here
    const token = admin.generateAuthToken();

    adminLogs = new AdminLog({
      email: admin.email,
      phoneNumber: admin.phoneNumber,
      formCode: "",
      incidentTitle: "",
      incidentDesc: "",
      activityType: "Reset",
      activityDesc: `This admin requested for forgot password`
    });

    await adminLogs.save();

    res.header('x-auth-token', token).send(jsonError(200, true, "New Password Generated Successfully", passwordGenerated));
  },

  resetAdminPassword: async (req, res) => {

    let admin = await Admin.findById(req.user._id);
    if (!admin) return res.status(404).send(jsonError(404, false, 'Admin not found.'));

    const validPassword = await bcrypt.compare(req.body.oldPassword, admin.password);
    if (!validPassword) return res.status(400).send(jsonError(400, false, 'Invalid request.'));

    const samePassword = await bcrypt.compare(req.body.password, admin.password);
    if (samePassword) return res.status(400).send(jsonError(400, false, 'You cannot use the old password as new password.'));

    const salt = await bcrypt.genSalt(10);
    const newPassword = await bcrypt.hash(req.body.password, salt);

    admin = await Admin.findByIdAndUpdate(req.user._id, {
      $set: {
        password: newPassword,
      }
    }, {
      new: true
    });

    admin = await Admin.findById(req.user._id).select('-password -isAdmin -__v');

    adminLogs = new AdminLog({
      email: admin.email,
      phoneNumber: admin.phoneNumber,
      formCode: "",
      incidentTitle: "",
      incidentDesc: "",
      activityType: "Reset",
      activityDesc: `This admin requested for password reset`
    });

    await adminLogs.save();

    res.status(200).send(jsonError(200, true, 'Password Reset Successful'));
  },

  changeAdminPassword: async (req, res) => {

    let admin = await Admin.findById(req.user._id);
    if (!admin) return res.status(404).send(jsonError(404, false, 'Admin not found.'));

    const samePassword = await bcrypt.compare(req.body.password, admin.password);
    if (samePassword) return res.status(400).send(jsonError(400, false, 'You cannot use the old password as new password.'));

    const salt = await bcrypt.genSalt(10);
    const newPassword = await bcrypt.hash(req.body.password, salt);

    admin = await Admin.findOneAndUpdate(req.user._id, {
      $set: {
        password: newPassword,
      }
    }, {
      new: true
    });

    admin = await Admin.findById(req.user._id).select('-password -isAdmin -__v');

    adminLogs = new AdminLog({
      email: admin.email,
      phoneNumber: admin.phoneNumber,
      formCode: "",
      incidentTitle: "",
      incidentDesc: "",
      activityType: "Update",
      activityDesc: `This admin changed password`
    });

    await adminLogs.save();

    res.status(200).send(jsonError(200, true, 'Password Change Successful'));
  },

  getAllAdmins: async (req, res) => {

    let admins = await Admin.find().select('-password -isAdmin -__v').sort({
      dateOfRegistration: -1
    });
    // if (!admin) return res.status(400).send(jsonError(400, false, 'Invalid email or password.'));

    res.status(200).send(jsonError(200, true, 'Admins retrieved', admins));
  },

  getOneAdmin: async (req, res) => {

    let admin = await Admin.findById(req.params.id).select('-password -isAdmin -__v');
    if (!admin) return res.status(404).send(jsonError(404, false, 'Admin not found.'));

    res.status(200).send(jsonError(200, true, 'Admin retrieved', admin));
  },

  deleteOneAdmin: async (req, res) => {

    let adminApprover = await Admin.findById(req.user._id);
    if (!adminApprover) return res.status(404).send(jsonError(404, false, 'Admin not found.'));

    let admin = await Admin.findById(req.params.id);
    if (!admin) return res.status(404).send(jsonError(404, false, 'Admin not found.'));

    admin = await Admin.findByIdAndDelete(req.params.id);
    if (!admin) return res.status(400).send(jsonError(400, false, 'Invalid request.'));

    adminLogs = new AdminLog({
      email: admin.email,
      phoneNumber: admin.phoneNumber,
      formCode: "",
      incidentTitle: "",
      incidentDesc: "",
      activityType: "Deletion",
      activityDesc: `This admin was deleted by ${adminApprover.email} ${adminApprover.phoneNumber}`
    });

    await adminLogs.save();

    res.status(200).send(jsonError(200, true, 'Admin deleted'));
  },

  setAdminPermission: async (req, res) => {

    let adminApprover = await Admin.findById(req.user._id);
    if (!adminApprover) return res.status(404).send(jsonError(404, false, 'Admin not found.'));

    let admin = await Admin.findById(req.params.id);
    if (!admin) return res.status(404).send(jsonError(404, false, 'Admin not found.'));

    console.log(req.params.id);
    console.log(admin);


    admin = await Admin.findByIdAndUpdate(req.params.id, {
      $set: {
        role: req.body.role,
      }
    }, {
      new: true
    });

    console.log(admin);

    admin = await Admin.findById(req.params.id).select('-password -isAdmin -__v');

    adminLogs = new AdminLog({
      email: admin.email,
      phoneNumber: admin.phoneNumber,
      formCode: "",
      incidentTitle: "",
      incidentDesc: "",
      activityType: "Permission",
      activityDesc: `This admin's permission was set by ${adminApprover.email} ${adminApprover.phoneNumber}`
    });

    await adminLogs.save();

    res.status(200).send(jsonError(200, true, `Permission of ${admin.firstName} Set As ${admin.role}`));
  },
}