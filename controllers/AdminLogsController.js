const _ = require('lodash');
const {
    AdminLog
} = require('../models/adminLogsModel');
const jsonError = require('../middlewares/jsonError');

module.exports = {

    getAllLogs: async (req, res) => {
        const logs = await AdminLog.find().sort('-dateOfLog');
        res.send(jsonError(200, true, `Logs Generated with total of ${logs.length}`, logs));
    },

    getOneLog: async (req, res) => {
        const log = await AdminLog.findOne({
            _id: req.params.id
        });

        if (!log) return res.status(404).send(jsonError(404, false, 'The log with the given id was not found.'));

        res.send(jsonError(200, true, 'Log found', log));
    },

}