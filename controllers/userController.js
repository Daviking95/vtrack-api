const bcrypt = require('bcryptjs');
// const argon2 = require('argon2');
const _ = require('lodash');
const {
  User
} = require('../models/userModel');
const jsonError = require('../middlewares/jsonError');
const randomString = require('../utils/randomString');

const UserController = {

  getCurrentUser: async (req, res) => {
    const user = await User.findById(req.user._id).select('-password -isAdmin -__v');
    if (!user) return res.status(404).send(jsonError(404, false, 'User not found.'));

    res.send(jsonError(200, true, 'User data gotten', user));
  },

  loginUser: async (req, res) => {

    let user = await User.findOne({
      email: req.body.email
    });
    if (!user) return res.status(400).send(jsonError(400, false, 'Invalid email or password.'));

    const validPassword = await bcrypt.compare(req.body.password, user.password);
    if (!validPassword) return res.status(400).send(jsonError(400, false, 'Invalid email or password.'));

    const token = user.generateAuthToken();
    res.status(200).send(jsonError(200, true, 'Login Successful', token));
  },

  registerUser: async (req, res) => {

    let user = await User.findOne({
      email: req.body.email
    });
    if (user) return res.status(400).send(jsonError(400, false, 'Email must be unique.'));

    if (await User.findOne({
        userName: req.body.userName
      })) return res.status(400).send(jsonError(400, false, 'Username must be unique.'));

    if (await User.findOne({
        phoneNumber: req.body.phoneNumber
      })) return res.status(400).send(jsonError(400, false, 'Phone number must be unique.'));

    user = new User(_.pick(req.body, ['userName', 'firstName', 'lastName', 'email', 'phoneNumber', 'birthday', 'password', 'gender']));
     const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);
    await user.save();

    const token = user.generateAuthToken();
    res.header('x-auth-token', token).send(jsonError(200, true, "Registration Successful", _.pick(user, ['_id', 'userName', 'email'])));
  },

  updateUserProfile: async (req, res) => {

    let user = await User.findById(req.user._id);
    if (!user) return res.status(404).send(jsonError(404, false, 'Invalid request.'));

    user = await User.findByIdAndUpdate(req.user._id, {
      $set: {
        userName: req.body.userName,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        phoneNumber: req.body.phoneNumber,
        gender: req.body.gender,
        birthday: req.body.birthday,
        picture: req.body.picture,
        address: req.body.address,
        city: req.body.city,
        state: req.body.state,
        country: req.body.country,

      }
    }, {
      new: true
    });

    user = await User.findById(req.user._id).select('-password -isAdmin -__v');

    res.status(200).send(jsonError(200, true, 'Profile Updated', user));
  },


  forgotUserPassword: async (req, res) => {

    let user = await User.findOne({
      email: req.body.email
    });
    if (!user) return res.status(404).send(jsonError(404, false, 'User not found.'));

    let passwordGenerated = randomString(20, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');

     const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(passwordGenerated, salt);

    user = await User.findByIdAndUpdate(user._id, {
      $set: {
        password: hashedPassword,
      }
    }, {
      new: true
    });

    // todo: Use SendGrid API to send new password Generated to user instead of here
    const token = user.generateAuthToken();
    res.header('x-auth-token', token).send(jsonError(200, true, "New Password Generated Successfully", passwordGenerated));
  },

  resetUserPassword: async (req, res) => {

    let user = await User.findById(req.user._id);
    if (!user) return res.status(404).send(jsonError(404, false, 'User not found.'));

    const validPassword = await bcrypt.compare(req.body.oldPassword, user.password);
    if (!validPassword) return res.status(400).send(jsonError(400, false, 'Invalid request.'));

    const samePassword = await bcrypt.compare(req.body.password, user.password);
    if (samePassword) return res.status(400).send(jsonError(400, false, 'You cannot use the old password as new password.'));

     const salt = await bcrypt.genSalt(10);
    const newPassword = await bcrypt.hash(req.body.password, salt);

    user = await User.findByIdAndUpdate(req.user._id, {
      $set: {
        password: newPassword,
      }
    }, {
      new: true
    });

    user = await User.findById(req.user._id).select('-password -isAdmin -__v');

    res.status(200).send(jsonError(200, true, 'Password Reset Successful'));
  },

  changeUserPassword: async (req, res) => {

    let user = await User.findById(req.user._id);
    if (!user) return res.status(404).send(jsonError(404, false, 'User not found.'));

    const samePassword = await bcrypt.compare(req.body.password, user.password);
    if (samePassword) return res.status(400).send(jsonError(400, false, 'You cannot use the old password as new password.'));

     const salt = await bcrypt.genSalt(10);
    const newPassword = await bcrypt.hash(req.body.password, salt);

    user = await User.findByIdAndUpdate(req.user._id, {
      $set: {
        password: newPassword,
      }
    }, {
      new: true
    });

    user = await User.findById(req.user._id).select('-password -isAdmin -__v');

    res.status(200).send(jsonError(200, true, 'Password Change Successful'));
  },

  getAllUsers: async (req, res) => {

    let users = await User.find().select('-password -isAdmin -__v').sort({
      dateOfRegistration: -1
    });
    // if (!user) return res.status(400).send(jsonError(400, false, 'Invalid email or password.'));

    res.status(200).send(jsonError(200, true, 'Users retrieved', users));
  },

  getAllUsersBirthdaysToday: async (req, res) => {

    let usersWithBirthdayToday = [];
    let d = new Date();

    let users = await User.find().select('-password -isAdmin -__v').sort({
      dateOfRegistration: -1
    });

    for (let index = 0; index < users.length; index++) {
      const element = users[index];

      if ((element.birthday.getDate() === d.getDate()) && (element.birthday.getMonth() === d.getMonth())) {
        usersWithBirthdayToday.push(element);
      }
    }

    res.status(200).send(jsonError(200, true, `Users with birthday on ${d.toLocaleDateString('default', { weekday: 'long' })}, ${d.toLocaleDateString()} retrieved`, usersWithBirthdayToday));
  },

  getAllUsersBirthdaysForCurrentMonth: async (req, res) => {

    let usersWithBirthdayMonth = [];
    let d = new Date();

    let users = await User.find().select('-password -isAdmin -__v').sort({
      dateOfRegistration: -1
    });

    for (let index = 0; index < users.length; index++) {
      const element = users[index];

      if ((element.birthday.getMonth() === d.getMonth())) {
        usersWithBirthdayMonth.push(element);
      }
    }

    res.status(200).send(jsonError(200, true, `Users with birthday in ${d.toLocaleString('default', { month: 'long' })} retrieved`, usersWithBirthdayMonth));
  },

  getOneUser: async (req, res) => {

    let user = await User.findById(req.params.id).select('-password -isAdmin -__v');
    if (!user) return res.status(404).send(jsonError(404, false, 'User not found.'));

    res.status(200).send(jsonError(200, true, 'User retrieved', user));
  },

  deleteOneUser: async (req, res) => {

    let user = await User.findById(req.params.id);
    if (!user) return res.status(404).send(jsonError(404, false, 'User not found.'));

    user = await User.findByIdAndDelete(req.params.id);
    if (!user) return res.status(400).send(jsonError(400, false, 'Invalid request.'));

    res.status(200).send(jsonError(200, true, 'User deleted'));
  },


}

module.exports = UserController;