const bcrypt = require('bcryptjs');
// const argon2 = require('argon2');
const _lod = require('lodash');
const {
  Monitor
} = require('../models/monitorModel');
const {
  AdminLog
} = require('../models/adminLogsModel');
const {
  Admin
} = require('../models/adminModel');
const jsonError = require('../middlewares/jsonError');
const randomString = require('../utils/randomString');

const newMonitorBody = ['user_monitor_code', 'user_name', 'user_first_name', 'user_last_name', 'user_email', 'user_phone_number', 'user_password', 'user_gender', 'state', 'country', 'reg_point', 'token_id', 'user_address'];

const MonitorController = {

  getCurrentMonitor: async (req, res) => {
    const monitor = await Monitor.findById(req.user._id).select('-user_password -isAdmin -__v');
    if (!monitor) return res.status(404).send(jsonError(404, false, 'Monitor not found.'));

    res.send(jsonError(200, true, 'Monitor data gotten', monitor));
  },

  loginMonitor: async (req, res) => {

    let monitor = await Monitor.findOne({
      user_monitor_code: req.body.user_monitor_code
    });
    if (!monitor) return res.status(400).send(jsonError(400, false, 'Invalid email or password.'));

    const validPassword = await bcrypt.compare(req.body.user_password, monitor.user_password);
    if (!validPassword) return res.status(400).send(jsonError(400, false, 'Invalid email or password.'));

    const token = monitor.generateAuthToken();

    adminLogs = new AdminLog({
      email: monitor.user_email,
      phoneNumber: monitor.user_monitor_code,
      formCode: "",
      incidentTitle: "",
      incidentDesc: "",
      activityType: "Authentication",
      activityDesc: `This monitor successfully logged in`
    });

    await adminLogs.save();

    res.status(200).send(jsonError(200, true, 'Login Successful', token));
  },

  registerMonitor: async (req, res) => {

    let adminLogs;
    let admin = await Admin.findById(req.user._id);
    if (!admin) return res.status(404).send(jsonError(404, false, 'Admin not found.'));

    let monitor = await Monitor.findOne({
      user_monitor_code: req.body.user_monitor_code
    });
    if (monitor) return res.status(400).send(jsonError(400, false, 'Monitor code must be unique.'));

    monitor = new Monitor(_lod.pick(req.body, newMonitorBody));
    const salt = await bcrypt.genSalt(10);
    monitor.user_password = await bcrypt.hash(monitor.user_password, salt);
    await monitor.save();

    const token = monitor.generateAuthToken();

    adminLogs = new AdminLog({
      email: monitor.user_email,
      phoneNumber: monitor.user_monitor_code,
      formCode: "",
      incidentTitle: "",
      incidentDesc: "",
      activityType: "Registration",
      activityDesc: `This monitor was successfully created by ${admin.email} ${admin.phoneNumber}`
    });

    await adminLogs.save();

    res.header('x-auth-token', token).send(jsonError(200, true, "Registration Successful", _lod.pick(monitor, ['_id', 'user_name', 'user_email'])));
  },

  registerMultipleMonitor: async (req, res) => {

    for (let index = 0; index < req.body.multiple.length; index++) {
      const element = req.body.multiple[index];

      let monitor = await Monitor.findOne({
        user_monitor_code: element.user_monitor_code
      });
      if (monitor) return res.status(400).send(jsonError(400, false, `Monitor code ${element.user_monitor_code} exists.`));

      monitor = new Monitor(_lod.pick(element, newMonitorBody));
      const salt = await bcrypt.genSalt(10);
      monitor.user_password = await bcrypt.hash(monitor.user_password, salt);
      await monitor.save();

      // const token = monitor.generateAuthToken();
      // res.header('x-auth-token', token).send(jsonError(200, true, "Registration Successful", _lod.pick(monitor, ['_id', 'user_name', 'user_email'])));

    }

    res.send(jsonError(200, true, "Monitor List has been added successfully"));

  },

  updateMonitorProfile: async (req, res) => {

    let monitor = await Monitor.findById(req.user._id);
    if (!monitor) return res.status(404).send(jsonError(404, false, 'Invalid request.'));

    monitor = await Monitor.findByIdAndUpdate(req.user._id, {
      $set: {
        user_name: req.body.user_name,
        user_first_name: req.body.user_first_name,
        user_last_name: req.body.user_last_name,
        user_email: req.body.user_email,
        user_description: req.body.user_description,
        user_phone_number: req.body.user_phone_number,
        user_gender: req.body.user_gender,
        user_age: req.body.user_age,
        user_address: req.body.user_address,
        state: req.body.state,
        country: req.body.country,

      }
    }, {
      new: true
    });

    monitor = await Monitor.findById(req.user._id).select('-user_password -isAdmin -__v');

    res.status(200).send(jsonError(200, true, 'Profile Updated', monitor));
  },

  updateOneMonitorProfile: async (req, res) => {

    let adminLogs;
    let admin = await Admin.findById(req.user._id);
    if (!admin) return res.status(404).send(jsonError(404, false, 'Admin not found.'));

    let monitor = await Monitor.findById(req.params.id);
    if (!monitor) return res.status(404).send(jsonError(404, false, 'Monitor not found.'));

    // if(await Monitor.findOne({
    //   user_monitor_code: req.body.user_monitor_code
    // })) return res.status(400).send(jsonError(400, false, 'Monitor code must be unique.'));

    monitor = await Monitor.findByIdAndUpdate(monitor._id, {
      $set: {
        user_name: req.body.user_name,
        user_first_name: req.body.user_first_name,
        user_last_name: req.body.user_last_name,
        user_email: req.body.user_email,
        user_description: req.body.user_description,
        user_phone_number: req.body.user_phone_number,
        user_gender: req.body.user_gender,
        user_age: req.body.user_age,
        user_address: req.body.user_address,
        state: req.body.state,
        country: req.body.country,

      }
    }, {
      new: true
    });

    monitor = await Monitor.findById(req.params.id).select('-user_password -isAdmin -__v');

    adminLogs = new AdminLog({
      email: req.body.user_email,
      phoneNumber: monitor.user_monitor_code,
      formCode: "",
      incidentTitle: "",
      incidentDesc: "",
      activityType: "Update",
      activityDesc: `This monitor was updated by ${admin.email}`
    });

    await adminLogs.save();

    res.status(200).send(jsonError(200, true, 'Profile Updated', monitor));
  },


  forgotMonitorPassword: async (req, res) => {

    let monitor = await Monitor.findOne({
      user_monitor_code: req.body.user_monitor_code
    });
    if (!monitor) return res.status(404).send(jsonError(404, false, 'Monitor not found.'));

    let passwordGenerated = randomString(20, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');

    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(passwordGenerated, salt);

    monitor = await Monitor.findByIdAndUpdate(monitor._id, {
      $set: {
        user_password: hashedPassword,
      }
    }, {
      new: true
    });

    // todo: Use SendGrid API to send new password Generated to monitor instead of here
    const token = monitor.generateAuthToken();
    res.header('x-auth-token', token).send(jsonError(200, true, "New Password Generated Successfully", {
      newPassword: passwordGenerated,
      token: token
    }));
  },

  resetMonitorPassword: async (req, res) => {

    let monitor = await Monitor.findById(req.user._id);
    if (!monitor) return res.status(404).send(jsonError(404, false, 'Monitor not found.'));

    const validPassword = await bcrypt.compare(req.body.oldPassword, monitor.user_password);
    if (!validPassword) return res.status(400).send(jsonError(400, false, 'Invalid request.'));

    const samePassword = await bcrypt.compare(req.body.password, monitor.user_password);
    if (samePassword) return res.status(400).send(jsonError(400, false, 'You cannot use the old password as new password.'));

    const salt = await bcrypt.genSalt(10);
    const newPassword = await bcrypt.hash(req.body.password, salt);

    monitor = await Monitor.findByIdAndUpdate(req.user._id, {
      $set: {
        user_password: newPassword,
      }
    }, {
      new: true
    });

    monitor = await Monitor.findById(req.user._id).select('-user_password -isAdmin -__v');

    res.status(200).send(jsonError(200, true, 'Password Reset Successful'));
  },

  changeMonitorPassword: async (req, res) => {

    let monitor = await Monitor.findById(req.user._id);
    if (!monitor) return res.status(404).send(jsonError(404, false, 'Monitor not found.'));

    const samePassword = await bcrypt.compare(req.body.password, monitor.user_password);
    if (samePassword) return res.status(400).send(jsonError(400, false, 'You cannot use the old password as new password.'));

    const salt = await bcrypt.genSalt(10);
    const newPassword = await bcrypt.hash(req.body.password, salt);

    monitor = await Monitor.findByIdAndUpdate(req.user._id, {
      $set: {
        user_password: newPassword,
      }
    }, {
      new: true
    });

    monitor = await Monitor.findById(req.user._id).select('-user_password -isAdmin -__v');

    res.status(200).send(jsonError(200, true, 'Password Change Successful'));
  },

  updateMonitorLocation: async (req, res) => {

    let monitor = await Monitor.findById(req.user._id);
    if (!monitor) return res.status(404).send(jsonError(404, false, 'Monitor not found.'));

    monitor = await Monitor.findByIdAndUpdate(req.user._id, {
      $set: {
        user_location: req.body.user_location,
      }
    }, {
      new: true
    });

    monitor = await Monitor.findById(req.user._id).select('-user_password -isAdmin -__v');

    adminLogs = new AdminLog({
      email: monitor.user_email,
      phoneNumber: monitor.user_monitor_code,
      formCode: "",
      incidentTitle: "",
      incidentDesc: "",
      activityType: "Update",
      activityDesc: `This monitor's location was updated to ${req.body.user_location}`
    });

    await adminLogs.save();

    res.status(200).send(jsonError(200, true, 'Monitor Location Updated Successfully'));
  },

  getAllMonitors: async (req, res) => {

    let monitors = await Monitor.find().select('-user_password -isAdmin -__v').sort({
      monitorCode: -1
    });
    // if (!monitor) return res.status(400).send(jsonError(400, false, 'Invalid email or password.'));

    res.status(200).send(jsonError(200, true, `Monitor list retrieved with total of ${monitors.length} monitors`, monitors));
  },

  getOneMonitor: async (req, res) => {

    let monitor = await Monitor.findById(req.params.id).select('-user_password -isAdmin -__v');
    if (!monitor) return res.status(404).send(jsonError(404, false, 'Monitor not found.'));

    res.status(200).send(jsonError(200, true, 'Monitor retrieved', monitor));
  },

  deleteOneMonitor: async (req, res) => {

    let adminLogs;
    let admin = await Admin.findById(req.user._id);
    if (!admin) return res.status(404).send(jsonError(404, false, 'Admin not found.'));

    let monitor = await Monitor.findById(req.params.id);
    if (!monitor) return res.status(404).send(jsonError(404, false, 'Monitor not found.'));

    monitor = await Monitor.findByIdAndDelete(req.params.id);
    if (!monitor) return res.status(400).send(jsonError(400, false, 'Invalid request.'));

    adminLogs = new AdminLog({
      email: monitor.user_email,
      phoneNumber: monitor.user_monitor_code,
      formCode : "",
      incidentTitle : "",
      incidentDesc: "",
      activityType: "Deletion",
      activityDesc : `This monitor was deleted by ${admin.email} ${admin.phoneNumber}`
  });
  
  await adminLogs.save();

    res.status(200).send(jsonError(200, true, 'Monitor deleted'));
  },


}

module.exports = MonitorController;