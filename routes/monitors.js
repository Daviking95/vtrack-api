const auth = require('../middlewares/auth');
const admin = require('../middlewares/admin');
const adminVerifier = require('../middlewares/adminVerifier');
const adminApprover = require('../middlewares/adminApprover');
const express = require('express');
const router = express.Router();
const MonitorController = require('../controllers/monitorController');
const verifyMonitorInputs = require('../middlewares/verifyMonitorInputs');

router.get('/monitor/getCurrentMonitor', auth, MonitorController.getCurrentMonitor);

router.post('/monitor/auth', verifyMonitorInputs.validateMonitorLoginReqBody, MonitorController.loginMonitor );

router.post('/monitor/register', [auth, admin], verifyMonitorInputs.validateMonitorRegisterReqBody, MonitorController.registerMonitor);

router.post('/monitor/register-multiple', [auth, admin], verifyMonitorInputs.validateMultipleMonitorRegisterReqBody, MonitorController.registerMultipleMonitor);

router.patch('/monitor/update', [auth], verifyMonitorInputs.validateMonitorUpdateReqBody, MonitorController.updateMonitorProfile);

router.put('/monitor/update', [auth], verifyMonitorInputs.validateMonitorUpdateReqBody, MonitorController.updateMonitorProfile);

router.put('/monitor/one/update/:id', [auth], verifyMonitorInputs.validateMonitorUpdateReqBody, MonitorController.updateOneMonitorProfile);

router.patch('/monitor/updateLocation', [auth], verifyMonitorInputs.validateMonitorUpdateLocationReqBody, MonitorController.updateMonitorLocation);

router.patch('/monitor/forgotPassword', verifyMonitorInputs.validateForgotPasswordReqBody, MonitorController.forgotMonitorPassword);

router.patch('/monitor/resetPassword', auth, verifyMonitorInputs.validateResetPasswordReqBody, MonitorController.resetMonitorPassword);

router.patch('/monitor/changePassword', [auth], verifyMonitorInputs.validateChangePasswordReqBody, MonitorController.changeMonitorPassword);

router.get('/monitor/all', [auth, admin], MonitorController.getAllMonitors);

router.get('/monitor/one/:id', [auth, admin], MonitorController.getOneMonitor);

router.delete('/monitor/one/:id', [auth, adminApprover], MonitorController.deleteOneMonitor);

module.exports = router; 
