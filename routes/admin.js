const express = require('express');
const router = express.Router();
const auth = require('../middlewares/auth');
const admin = require('../middlewares/admin');
const adminApprover = require('../middlewares/adminApprover');
const AdminController = require('../controllers/adminController');
const verifyAdminInputs = require('../middlewares/verifyAdminInputs');

router.post('/admin/create', verifyAdminInputs.validateAdminCreateReqBody, AdminController.registerAdmin);

router.get('/admin/getCurrentAdmin', auth, AdminController.getCurrentAdmin);

router.post('/admin/create-verifier', [auth, adminApprover], verifyAdminInputs.validateAdminCreateReqBody, AdminController.registerAdmin);

router.post('/admin/auth', verifyAdminInputs.validateAdminLoginReqBody, AdminController.loginAdmin );

router.patch('/admin/update', [auth, admin], verifyAdminInputs.validateAdminUpdateReqBody, AdminController.updateAdminProfile);

router.patch('/admin/one/update/:id', [auth, admin], verifyAdminInputs.validateOneAdminUpdateReqBody, AdminController.updateOneAdminProfile);

router.patch('/admin/forgotPassword', verifyAdminInputs.validateForgotPasswordReqBody, AdminController.forgotAdminPassword);

router.patch('/admin/resetPassword', [auth, admin], verifyAdminInputs.validateResetPasswordReqBody, AdminController.resetAdminPassword);

router.patch('/admin/changePassword', [auth, admin], verifyAdminInputs.validateChangePasswordReqBody, AdminController.changeAdminPassword);

router.get('/admin/all', [auth, admin], AdminController.getAllAdmins);

router.get('/admin/one/:id', [auth, admin], AdminController.getOneAdmin);

router.delete('/admin/one/:id', [auth, adminApprover], AdminController.deleteOneAdmin);

router.patch('/admin/set-permission/:id', [auth, adminApprover], verifyAdminInputs.validateAdminSetPermission, AdminController.setAdminPermission );


module.exports = router; 
