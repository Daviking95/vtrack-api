const auth = require('../middlewares/auth');
const admin = require('../middlewares/admin');
const adminVerifier = require('../middlewares/adminVerifier');
const adminApprover = require('../middlewares/adminApprover');
const express = require('express');
const router = express.Router();
const UserController = require('../controllers/userController');
const verifyUserInputs = require('../middlewares/verifyUserInputs');

router.get('/user/getCurrentUser', auth, UserController.getCurrentUser);

router.post('/auth', verifyUserInputs.validateUserLoginReqBody, UserController.loginUser );

router.post('/user/register', verifyUserInputs.validateUserRegisterReqBody, UserController.registerUser);

router.patch('/user/update', auth, verifyUserInputs.validateUserUpdateReqBody, UserController.updateUserProfile);

router.patch('/user/forgotPassword', verifyUserInputs.validateForgotPasswordReqBody, UserController.forgotUserPassword);

router.patch('/user/resetPassword', auth, verifyUserInputs.validateResetPasswordReqBody, UserController.resetUserPassword);

router.patch('/user/changePassword', [auth], verifyUserInputs.validateChangePasswordReqBody, UserController.changeUserPassword);

router.get('/user/all', [auth, admin], UserController.getAllUsers);

router.get('/user/one/:id', [auth, admin], UserController.getOneUser);

router.get('/user/birthdays/today', UserController.getAllUsersBirthdaysToday);

router.get('/user/birthdays/currentMonth', UserController.getAllUsersBirthdaysForCurrentMonth);

router.delete('/user/one/:id', [auth, adminApprover], UserController.deleteOneUser);

module.exports = router; 
