const auth = require('../middlewares/auth');
const admin = require('../middlewares/admin');
const adminVerifier = require('../middlewares/adminVerifier');
const adminApprover = require('../middlewares/adminApprover');
const express = require('express');
const router = express.Router();
const AdminLogsController = require('../controllers/adminLogsController');

router.get('/logs/all', [auth, admin], AdminLogsController.getAllLogs);

router.get('/logs/one/:id', [auth, admin], AdminLogsController.getOneLog);

module.exports = router; 
