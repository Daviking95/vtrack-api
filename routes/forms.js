const auth = require('../middlewares/auth');
const admin = require('../middlewares/admin');
const adminVerifier = require('../middlewares/adminVerifier');
const adminApprover = require('../middlewares/adminApprover');
const express = require('express');
const router = express.Router();
const FormController = require('../controllers/formController');
const verifyFormInputs = require('../middlewares/verifyFormInputs');

router.post('/form/add', verifyFormInputs.validateFormReqBody, FormController.addForm);

router.post('/form/add-multiple', [auth], verifyFormInputs.validateMultipleFormReqBody, FormController.addMultipleForms);

router.get('/form/all', FormController.getAllForms);

router.get('/form/get-awaiting-verification', [auth, admin], FormController.getAwaitingVerificationForms);

router.get('/form/get-form-by-type-value/:type/:value', FormController.getFormByTypeAndValueFromForms);

router.get('/form/get-awaiting-approval', [auth, adminApprover], FormController.getAwaitingApprovalForms);

router.get('/form/one/:id', FormController.getOneForm);

router.get('/form/user', auth, FormController.getUserForms);

router.put('/form/update/:id', [auth, admin], verifyFormInputs.validateFormReqBody, FormController.updateForm);

router.put('/form/verify/:id', [auth, adminVerifier], verifyFormInputs.validateVerifyFormReqBody, FormController.verifyForm);

router.put('/form/approve/:id', [auth, adminApprover], verifyFormInputs.validateApproveFormReqBody, FormController.approveForm);

router.delete('/form/one/:id', [auth, adminApprover], FormController.deleteOneForm);

module.exports = router; 
