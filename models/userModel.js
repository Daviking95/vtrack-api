const config = require("config");
const jwt = require("jsonwebtoken");
const Joi = require('joi');
const PasswordComplexity = require('joi-password-complexity');
const mongoose = require('mongoose');

const roles = ['user'];
const gender = ['male', 'female', 'unknown'];

const userSchema = new mongoose.Schema({
  userName: {
    type: String,
    default : '',
    trim: true,
    lowercase: true,
    unique: true,
    maxlength: 50
  },
  firstName: {
    type: String,
    default : '',
    trim: true,
    lowercase: true,
    maxlength: 50
  },
  lastName: {
    type: String,
    default : '',
    trim: true,
    lowercase: true,
    maxlength: 50
  },
  email: {
    type: String,
    unique: true,
    match: /^\S+@\S+\.\S+$/,
    trim: true,
    lowercase: true,
    minlength: 5,
    maxlength: 255,
    required:true
  },
  phoneNumber: {
    type: String,
    unique: true,
    required: true,
    minlength: 11,
    maxlength: 50
  },
  gender: {
    type: String,
    enum: gender,
    required: true,
    lowercase:true,
    maxlength: 50,
  },
  birthday: {
    type: Date,
    required : true,
    minlength: 8,
    maxlength: 1024
  },
  picture: {
    type: String,
    trim: true,
    default: null,
    maxlength: 255,
  },
  password: {
    type: String,
    required: true,
    minlength: 8,
    maxlength: 1024
  },
  address: {
    type: String,
    default : null,
    lowercase: true,
    maxlength: 1024
  },
  city: {
    type: String,
    default : null,
    lowercase: true,
    maxlength: 1024
  },
  state: {
    type: String,
    default : null,
    lowercase: true,
    maxlength: 1024
  },
  country: {
    type: String,
    lowercase: true,
    maxlength: 1024
  },
  isAdmin: {
    type: Boolean,
    default: false,
    maxlength: 50,
  },
  isUserSubscribed : {
    type: Boolean,
    default: true,
    maxlength: 50,
  },
  role: {
    type: String,
    enum: roles,
    default: 'user',
    lowercase:true,
    maxlength: 50,
  },
  dateOfRegistration: {
    type: Date,
    default: Date.now()
  },
}); 

userSchema.methods.generateAuthToken = function(){ 
  const token = jwt.sign({_id : this._id, isAdmin : this.isAdmin}, process.env.JWT_SECRET); 
  return token;
};

const User = mongoose.model('User', userSchema);

exports.User = User; 