const config = require("config");
const jwt = require("jsonwebtoken");
const Joi = require('joi');
const PasswordComplexity = require('joi-password-complexity');
const mongoose = require('mongoose');
require('dotenv').config();

const roles = ['approver', 'verifier'];

const adminSchema = new mongoose.Schema({
  firstName: {
    type: String,
    default : '',
    trim: true,
    lowercase: true,
    maxlength: 50
  },
  lastName: {
    type: String,
    default : '',
    trim: true,
    lowercase: true,
    maxlength: 50
  },
  email: {
    type: String,
    unique: true,
    match: /^\S+@\S+\.\S+$/,
    trim: true,
    lowercase: true,
    minlength: 5,
    maxlength: 255,
    required:true
  },
  phoneNumber: {
    type: String,
    unique: true,
    required: true,
    minlength: 11,
    maxlength: 50
  },
  picture: {
    type: String,
    trim: true,
    maxlength: 255,
  },
  password: {
    type: String,
    required: true,
    minlength: 8,
    maxlength: 1024
  },
  isAdmin: {
    type: Boolean,
    default: true,
    maxlength: 50,
  },
  role: {
    type: String,
    enum: roles,
    default: 'verifier',
    lowercase:true,
    maxlength: 50,
  },
  dateOfRegistration: {
    type: Date,
    default: Date.now()
  },
}); 

adminSchema.methods.generateAuthToken = function(){ 
  const token = jwt.sign({_id : this._id, isAdmin : this.isAdmin, role: this.role}, process.env.JWT_SECRET); 
  return token;
};

const Admin = mongoose.model('Admin', adminSchema);

exports.Admin = Admin; 