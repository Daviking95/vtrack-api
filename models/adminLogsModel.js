const config = require("config");
const jwt = require("jsonwebtoken");
const Joi = require('joi');
const PasswordComplexity = require('joi-password-complexity');
const mongoose = require('mongoose');
require('dotenv').config();

const adminLogSchema = new mongoose.Schema({
    email: {
        type: String,
        match: /^\S+@\S+\.\S+$/,
        trim: true,
        lowercase: true,
        // minlength: 5,
        maxlength: 255,
    },
    phoneNumber: {
        type: String,
        // minlength: 11,
        maxlength: 50
    },
    formCode: {
        type: String,
        trim: true,
        lowercase: true,
        // unique: true,
        maxlength: 1024
    },
    incidentTitle: {
        type: String,
        trim: true,
        lowercase: true,

        maxlength: 1024
    },
    incidentDesc: {
        type: String,
        trim: true,
        lowercase: true,

        // maxlength: 1024
    },
    activityType: {
        type: String,
        trim: true,
        lowercase: true,
    },
    activityDesc: {
        type: String,
        trim: true,
        lowercase: true,
    },
    dateOfLog: {
        type: Date,
        default: Date.now
    },
});

const AdminLog = mongoose.model('AdminLog', adminLogSchema);

exports.AdminLog = AdminLog;