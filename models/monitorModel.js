const config = require("config");
const jwt = require("jsonwebtoken");
const Joi = require('joi');
const PasswordComplexity = require('joi-password-complexity');
const mongoose = require('mongoose');

const gender = ['male', 'female', 'unknown'];
const regPoint = ['web', 'mobile', 'sms'];

const monitorSchema = new mongoose.Schema({
    user_monitor_code: {
        type: String,
        lowercase: true,
        unique: true,
        maxlength: 1024
    },
    user_name: {
        type: String,
        default: '',
        trim: true,
        lowercase: true,
        maxlength: 50
    },
    user_first_name: {
        type: String,
        default: '',
        trim: true,
        lowercase: true,
        maxlength: 50
    },
    user_last_name: {
        type: String,
        default: '',
        trim: true,
        lowercase: true,
        maxlength: 50
    },
    user_email: {
        type: String,
        // unique: true,
        match: /^\S+@\S+\.\S+$/,
        trim: true,
        lowercase: true,
        minlength: 5,
        maxlength: 255,
        required: true
    },
    user_phone_number: {
        type: String,
        // unique: true,
        minlength: 5,
        maxlength: 50
    },
    user_gender: {
        type: String,
        enum: gender,
        required: true,
        lowercase: true,
        maxlength: 50,
    },
    user_age: {
        type: String,
        trim: true,
        default: null,
        maxlength: 255,
    },
    user_password: {
        type: String,
        required: true,
        minlength: 8,
        maxlength: 1024
    },
    user_address: {
        type: String,
        default: null,
        lowercase: true,
        maxlength: 1024
    },
    user_description: {
        type: String,
        default: null,
        lowercase: true,
        maxlength: 1024
    },
    user_image: {
        type: String,
        default: "gs://kimpact-development-initiative.appspot.com/logo_one.png",
        lowercase: true,
        maxlength: 1024
    },
    user_location: {
        type: String,
        default: "",
        lowercase: true,
        maxlength: 1024
    },
    reg_point: {
        type: String,
        enum: regPoint,
        default: "web",
        lowercase: true,
        maxlength: 1024,
    },
    token_id: {
        type: String,
        default: null,
        lowercase: true,
        maxlength: 1024,
    },
    state: {
        type: String,
        default: null,
        lowercase: true,
        maxlength: 1024,
    },
    country: {
        type: String,
        default: null,
        lowercase: true,
        maxlength: 1024,
    },
    isAdmin: {
        type: Boolean,
        default: false,
        maxlength: 50,
    },
    dateOfRegistration: {
        type: Date,
        default: Date.now()
    },
});

monitorSchema.methods.generateAuthToken = function () {
    const token = jwt.sign({
        _id: this._id,
        user_monitor_code: this.user_monitor_code,
        isAdmin: this.isAdmin
    }, process.env.JWT_SECRET);
    return token;
};

const Monitor = mongoose.model('Monitor', monitorSchema);

exports.Monitor = Monitor;