const Joi = require('joi');
const mongoose = require('mongoose');

const formSubmissionPoint = ['web', 'app', 'sms'];

const formSchema = new mongoose.Schema({
    monitorName: {
        type: String,
        trim: true,
        // lowercase: true,    
        // maxlength: 255
    },
    monitorCode: {
        type: String,
        // required: true,
        trim: true,
        // lowercase: true,     
        // maxlength: 1024
    },
    dateOfReport: {
        type: String,
        trim: true,
        // lowercase: true,    
        // maxlength: 1024
    },
    formCode: {
        type: String,
        // required: true,
        trim: true,
        // lowercase: true,
        // unique: true,
        // maxlength: 1024
    },
    formType: {
        type: String,
        trim: true,
        // lowercase: true,
        // maxlength: 1024
    },
    electionType: {
        type: String,
        trim: true,
        // lowercase: true,        
        // maxlength: 1024
    },
    dateOfIncidence: {
        type: String,
        trim: true,
        // lowercase: true,        
        // maxlength: 1024
    },
    timeOfIncidence: {
        type: String,
        trim: true,
        // lowercase: true,       
        // maxlength: 1024
    },
    geoPoliticalZone: {
        type: String,
        trim: true,
        // lowercase: true, 
        // maxlength: 1024
    },
    city: {
        type: String,
        trim: true,
        // lowercase: true,
        // maxlength: 1024
    },
    state: {
        type: String,
        trim: true,
        // lowercase: true,
        // maxlength: 1024
    },
    incidentTitle: {
        type: String,
        required: true,
        trim: true,
        // lowercase: true,
        
        // maxlength: 1024
    },
    incidentDesc: {
        type: String,
        trim: true,
        // lowercase: true,
        
        // maxlength: 1024
    },
    incidentSource: {
        type: Array,
        trim: true,
        maxlength: 255
    },
    mediaSource: {
        type: Array,
        // lowercase: true,
    },
    incidentLocation: {
        type: Map,
        // required: true
    },
    violenceInitiatorPeopleAndGenderInvolved: {
        type: Map,
        // required: true
    },
    violenceInitiator: {
        type: Array,
        // lowercase: true,
    },
    violenceVictimPeopleAndGenderInvolved: {
        type: Map,
        // lowercase: true,
    },
    incidentViolenceVictims: {
        type: Array,
        // lowercase: true,
    },
    incidentViolence: {
        type: Array,
        // lowercase: true,
    },
    incidentWeapon: {
        type: Array,
        // lowercase: true,
    },
    incidentImpact: {
        type: Array,
        // lowercase: true,
    },
    afterIncident: {
        type: Map,
        // lowercase: true,
    },
    moreReportDetails: {
        type: Map,
        // lowercase: true,
    },
    mediaAsset: {
        type: Array,
        // lowercase: true,
    },
    timeOfReport: {
        type: String,
        trim: true,
        // lowercase: true,
        
        maxlength: 1024
    },
    locationOfReport: {
        type: String,
        trim: true,
        default: null,
        // lowercase: true,
        
    },
    firstApproval: {
        type: Boolean,
        default: false,
        maxlength: 50,
    },
    secondApproval: {
        type: Boolean,
        default: false,
        maxlength: 50,
    },
    formSubmissionPoint: {
        type: String,
        enum: formSubmissionPoint,
        default: "app",
        trim: true,
        maxlength: 255
    },
    dateAdded: {
        type: Date,
        required: true,
        default: Date.now
    }
});

const Form = mongoose.model('Form', formSchema);

exports.formSchema = formSchema;
exports.Form = Form;