const Joi = require('joi');
const jsonError = require('./jsonError');

const incidentSourceBody = Joi.object().keys({
    didSourceSeeIncident: Joi.string().allow(""),
    incidentSourceOption: Joi.string(),
    incidentSourceOptionPosition: Joi.number(),
});

const mediaSourceBody = Joi.object().keys({
    nameOne: Joi.string().allow(""),
    nameTwo: Joi.string().allow(""),
    nameThree: Joi.string().allow(""),
    mediaSourceOption: Joi.string(),
    mediaSourceOptionPosition: Joi.number(),
});

const incidentLocationBody = Joi.object().keys({
    locationOfIncident: Joi.string().allow(null, ''),
    specifylocationOfIncident: Joi.string().allow(""),
});

const violenceInitiatorPeopleAndGenderInvolvedBody = Joi.object().keys({
    violenceInitiatorGenderOfIncident: Joi.string().allow(""),
    violenceInitiatorPeopleOfIncident: Joi.string().allow(""),
});

const violenceInitiatorBody = Joi.object().keys({
    violenceInitiatorDesc: Joi.string().allow(""),
    violenceInitiatorType: Joi.string().allow(""),
    violenceInitiatorPosition: Joi.number().allow(""),
});

const violenceVictimPeopleAndGenderInvolvedBody = Joi.object().keys({
    violenceVictimsGenderOfIncident: Joi.string().allow(""),
    violenceVictimTypeOfIncident: Joi.string().allow(""),
    violenceVictimsPeopleOfIncident: Joi.string().allow(""),
});

const incidentViolenceVictimsBody = Joi.object().keys({
    violenceVictimsDesc: Joi.string().allow(""),
    violenceVictimsType: Joi.string().allow(""),
    incidentViolenceVictimsPosition: Joi.number(),
});

const incidentViolenceBody = Joi.object().keys({
    violenceDesc: Joi.string().allow(""),
    violenceType: Joi.string().allow(""),
    incidentViolencePosition: Joi.number(),
});

const incidentWeaponBody = Joi.object().keys({
    weaponDesc: Joi.string().allow(""),
    weaponType: Joi.string().allow(""),
    incidentWeaponPosition: Joi.number(),
});

const incidentImpactBody = Joi.object().keys({
    impactDesc: Joi.string().allow(""),
    impactType: Joi.string().allow(""),
    incidentImpactPosition: Joi.number(),
});

const afterIncidentBody = Joi.object().keys({
    afterIncidentDetails: Joi.string().allow(""),
    afterIncidentOption: Joi.string().allow(""),
});

const moreReportDetailsBody = Joi.object().keys({
    moreReportDetailsOption: Joi.string().allow(""),
});

const formBody = {

    formType: Joi.string().max(1024).allow(""),
    monitorName: Joi.string().max(1024).allow(""),
    monitorCode: Joi.string().max(1024).allow(""),
    dateOfReport: Joi.string().max(1024).allow(""),
    formCode: Joi.string().max(1024).allow(""),
    electionType: Joi.string().max(1024).allow(""),
    dateOfIncidence: Joi.string().max(1024).allow(""),
    timeOfIncidence: Joi.string().max(1024).allow(""),
    geoPoliticalZone: Joi.string().max(1024).allow(""),
    city: Joi.string().max(1024).allow(""),
    state: Joi.string().max(1024).allow(""),
    incidentTitle: Joi.string().max(1024),
    incidentDesc: Joi.string().allow(""),
    incidentSource: Joi.array().items(incidentSourceBody),
    mediaSource: Joi.array().items(mediaSourceBody),
    incidentLocation: incidentLocationBody,
    violenceInitiatorPeopleAndGenderInvolved: violenceInitiatorPeopleAndGenderInvolvedBody,
    violenceInitiator: Joi.array().items(violenceInitiatorBody),
    violenceVictimPeopleAndGenderInvolved: violenceVictimPeopleAndGenderInvolvedBody,
    incidentViolenceVictims: Joi.array().items(incidentViolenceVictimsBody),
    incidentViolence: Joi.array().items(incidentViolenceBody),
    incidentWeapon: Joi.array().items(incidentWeaponBody),
    incidentImpact: Joi.array().items(incidentImpactBody),
    afterIncident: afterIncidentBody,
    moreReportDetails: moreReportDetailsBody,
    mediaAsset: Joi.array().items(Joi.string().allow(null)),
    timeOfReport: Joi.string().max(1024),
    locationOfReport: Joi.string().allow(null, ''),
    formSubmissionPoint: Joi.string().valid('web', 'app', 'sms').max(255),
    firstApproval: Joi.boolean(),
    secondApproval: Joi.boolean(),


}

const verifyFormInputs = {
    validateFormReqBody: (req, res, next) => {

        const schema = formBody;

        const {
            error
        } = Joi.validate(req.body, schema);
        if (error) return res.status(400).send(jsonError(400, false, error.details[0].message));
        else next();
    },

    validateMultipleFormReqBody: (req, res, next) => {

        const schemaBody = Joi.object().keys(formBody);
    
        const schema = {
          multiple: Joi.array().items(schemaBody).min(1),
      };
    
        const {
          error
        } = Joi.validate(req.body, schema);
        if (error) return res.status(400).send(jsonError(400, false, error.details[0].message));
        else next();
      },

      validateVerifyFormReqBody: (req, res, next) => {

        const schema = {
            firstApproval: Joi.boolean(),
        };
    
        const { error } =  Joi.validate(req.body, schema);
        if (error) return res.status(400).send(jsonError(400, false, error.details[0].message));
        else next();
      },

      validateApproveFormReqBody: (req, res, next) => {

        const schema = {
            secondApproval: Joi.boolean(),
        };
    
        const { error } =  Joi.validate(req.body, schema);
        if (error) return res.status(400).send(jsonError(400, false, error.details[0].message));
        else next();
      },
}

module.exports = verifyFormInputs;