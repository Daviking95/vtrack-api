module.exports = function(statusCode, status, message, details){ 
    return {statusCode: statusCode,
    status: status,
    message: message,
    details: details }
  };