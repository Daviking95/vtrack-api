const Joi = require('joi');
const jsonError = require('./jsonError');

module.exports = {

  validateAdminLoginReqBody: (req, res, next) => {

    const schema = {
      email: Joi.string().min(5).max(255).required().email(),
      password: Joi.string().min(5).max(255).required()
    };

    const {
      error
    } = Joi.validate(req.body, schema);
    if (error) return res.status(400).send(jsonError(400, false, error.details[0].message));
    else next();
  },

  validateAdminCreateReqBody: (req, res, next) => {

    const complexityOptions = {
      min: 10,
      max: 30,
      lowerCase: 1,
      upperCase: 1,
      numeric: 1,
      symbol: 1,
      requirementCount: 2,
    }

    const schema = {
      firstName: Joi.string().max(50).required(),
      lastName: Joi.string().max(50).required(),
      email: Joi.string().min(5).max(255).required().exist().email(),
      phoneNumber: Joi.string().min(11).max(50).regex(/^\d{4}-\d{3}-\d{4}$/).required().exist(),
      password: Joi.string().min(8).max(255).regex(/^[a-zA-Z0-9]{8,30}$/).required(),
      passwordConfirmation: Joi.any().equal(Joi.ref('password')).required(),
      role: Joi.string().valid('approver', 'verifier').max(50).required(),
    };

    const {
      error
    } = Joi.validate(req.body, schema);
    if (error) return res.status(400).send(jsonError(400, false, error.details[0].message));
    else next();
  },

  validateAdminUpdateReqBody: (req, res, next) => {

    const schema = {
      firstName: Joi.string().max(50).required().empty(),
      lastName: Joi.string().max(50).required().empty(),
      email: Joi.string().min(5).max(255).required().empty().email(),
      phoneNumber: Joi.string().min(11).max(50).regex(/^\d{4}-\d{3}-\d{4}$/).required().empty(),
      picture: Joi.string().max(255).required().allow(""),

    };
  
    const{error} =  Joi.validate(req.body, schema);
    if (error) return res.status(400).send(jsonError(400, false, error.details[0].message));
    else next();
  },

  validateOneAdminUpdateReqBody: (req, res, next) => {

    const schema = {
      firstName: Joi.string().max(50).required().empty(),
      lastName: Joi.string().max(50).required().empty(),
      email: Joi.string().min(5).max(255).required().empty().email(),
      phoneNumber: Joi.string().min(11).max(50).regex(/^\d{4}-\d{3}-\d{4}$/).required().empty(),
      picture: Joi.string().max(255).required().allow(""),
      role: Joi.string().max(50).required().empty(),

    };
  
    const{error} =  Joi.validate(req.body, schema);
    if (error) return res.status(400).send(jsonError(400, false, error.details[0].message));
    else next();
  },

  validateForgotPasswordReqBody: (req, res, next) => {

    const schema = {
      email: Joi.string().min(5).max(255).required().empty().email(),
    };

    const { error } =  Joi.validate(req.body, schema);
    if (error) return res.status(400).send(jsonError(400, false, error.details[0].message));
    else next();
  },

  validateResetPasswordReqBody: (req, res, next) => {

    const schema = {
      oldPassword: Joi.string().min(8).max(255).regex(/^[a-zA-Z0-9]{8,30}$/).required(),
      password: Joi.string().min(8).max(255).regex(/^[a-zA-Z0-9]{8,30}$/).required(),
      passwordConfirmation: Joi.any().equal(Joi.ref('password')).required(),
    };
  
    const{error} =  Joi.validate(req.body, schema);
    if (error) return res.status(400).send(jsonError(400, false, error.details[0].message));
    else next();
  },

  validateChangePasswordReqBody: (req, res, next) => {

    const schema = {
      password: Joi.string().min(8).max(255).regex(/^[a-zA-Z0-9]{8,30}$/).required(),
      passwordConfirmation: Joi.any().equal(Joi.ref('password')).required(),
    };
  
    const{error} =  Joi.validate(req.body, schema);
    if (error) return res.status(400).send(jsonError(400, false, error.details[0].message));
    else next();
  },

  validateAdminSetPermission: (req, res, next) => {

    const schema = {
      role: Joi.string().valid('approver', 'verifier').max(50).required(),
    };

    const {
      error
    } = Joi.validate(req.body, schema);
    if (error) return res.status(400).send(jsonError(400, false, error.details[0].message));
    else next();
  },
}