const jsonError = require('./jsonError');

module.exports = function (req, res, next) {

  if (!req.user.isAdmin) return res.status(403).send(jsonError(403, false, 'Access denied. You are not an admin.'));

  next();
}