const Joi = require('joi');
const jsonError = require('./jsonError');

const newMonitorObjectBody = {
  user_monitor_code: Joi.string().max(1024).exist().required(),
  user_name: Joi.string().max(1024).allow(""),
  user_first_name: Joi.string().max(1024).required(),
  user_last_name: Joi.string().max(1024).required(),
  user_description: Joi.string().max(1024).allow(""),
  user_email: Joi.string().min(5).max(1024).required().email(), // .exist()
  user_phone_number: Joi.string().max(1024).allow(""), // format XXXX-XXX-XXXX  // .exist()
  user_password: Joi.string().min(5).max(1024).required(),
  user_gender: Joi.string().valid('male', 'female', 'unknown').max(1024).required(),
  user_age: Joi.string().max(1024).allow(""),
  state: Joi.string().max(1024).allow(""),
  country: Joi.string().max(1024).allow(""),
  reg_point: Joi.string().max(1024).allow(""),
  token_id: Joi.string().max(1024).allow(""),
  user_address: Joi.string().max(1024).allow(""),
  user_image: Joi.string().max(1024).allow(""),
  user_location: Joi.string().max(1024).allow(""),

};

const verifyMonitorInputs = {
  validateMonitorLoginReqBody: (req, res, next) => {

    const schema = {
      user_monitor_code: Joi.string().min(5).max(1024).required(),
      user_password: Joi.string().min(5).max(1024).required()
    };

    const {
      error
    } = Joi.validate(req.body, schema);
    if (error) return res.status(400).send(jsonError(400, false, error.details[0].message));
    else next();
  },

  validateMonitorRegisterReqBody: (req, res, next) => {

    const schema = {
      user_monitor_code: Joi.string().max(1024).exist().required(),
      user_name: Joi.string().max(1024).allow(""),
      user_first_name: Joi.string().max(1024).required(),
      user_last_name: Joi.string().max(1024).required(),
      user_description: Joi.string().max(1024).allow(""),
      user_email: Joi.string().min(5).max(1024).required().exist().email(),
      user_phone_number: Joi.string().min(11).max(1024).exist().allow(""), // format XXXX-XXX-XXXX  
      user_password: Joi.string().min(8).max(1024).regex(/^[a-zA-Z0-9]{8,30}$/).required(), // Find a better reg
      user_password_confirm: Joi.any().equal(Joi.ref('user_password')).required(),
      user_gender: Joi.string().valid('male', 'female', 'unknown').max(1024).required(),
      user_age: Joi.string().max(1024).allow(""),
      state: Joi.string().max(1024).allow(""),
      country: Joi.string().max(1024).allow(""),
      reg_point: Joi.string().max(1024).allow(""),
      token_id: Joi.string().max(1024).allow(""),
      user_address: Joi.string().max(1024).allow(""),
      user_image: Joi.string().max(1024).allow(""),
      user_location: Joi.string().max(1024).allow(""),

    };

    const {
      error
    } = Joi.validate(req.body, schema);
    if (error) return res.status(400).send(jsonError(400, false, error.details[0].message));
    else next();
  },

  validateMultipleMonitorRegisterReqBody: (req, res, next) => {

    const schemaBody = Joi.object().keys(newMonitorObjectBody);

    const schema = {
      multiple: Joi.array().items(schemaBody).min(1).required(),
  };

    const {
      error
    } = Joi.validate(req.body, schema);
    if (error) return res.status(400).send(jsonError(400, false, error.details[0].message));
    else next();
  },

  validateMonitorUpdateReqBody: (req, res, next) => {

    const schema = {
      // user_monitor_code: Joi.string().max(1024).required(),
      user_name: Joi.string().max(1024).required(),
      user_first_name: Joi.string().max(1024).required(),
      user_last_name: Joi.string().max(1024).required(),
      user_email: Joi.string().min(5).max(1024).required().exist().email(),
      user_phone_number: Joi.string().min(11).max(1024).required().exist(),
      user_gender: Joi.string().valid('male', 'female', 'unknown').max(1024).required(),
      user_age: Joi.string().max(1024),
      user_address: Joi.string().max(1024),
      state: Joi.string().max(1024),
      country: Joi.string().max(1024),

    };

    const {
      error
    } = Joi.validate(req.body, schema);
    if (error) return res.status(400).send(jsonError(400, false, error.details[0].message));
    else next();
  },

  validateMonitorUpdateLocationReqBody: (req, res, next) => {

    const schema = {
      user_location: Joi.string().max(1024).required(),

    };

    const {
      error
    } = Joi.validate(req.body, schema);
    if (error) return res.status(400).send(jsonError(400, false, error.details[0].message));
    else next();
  },

  validateForgotPasswordReqBody: (req, res, next) => {

    const schema = {
      user_monitor_code: Joi.string().max(1024).required().empty(),
    };

    const {
      error
    } = Joi.validate(req.body, schema);
    if (error) return res.status(400).send(jsonError(400, false, error.details[0].message));
    else next();
  },

  validateResetPasswordReqBody: (req, res, next) => {

    const schema = {
      oldPassword: Joi.string().min(8).max(1024).regex(/^[a-zA-Z0-9]{8,30}$/).required(),
      password: Joi.string().min(8).max(1024).regex(/^[a-zA-Z0-9]{8,30}$/).required(),
      passwordConfirmation: Joi.any().equal(Joi.ref('password')).required(),
    };

    const {
      error
    } = Joi.validate(req.body, schema);
    if (error) return res.status(400).send(jsonError(400, false, error.details[0].message));
    else next();
  },

  validateChangePasswordReqBody: (req, res, next) => {

    const schema = {
      password: Joi.string().min(8).max(1024).regex(/^[a-zA-Z0-9]{8,30}$/).required(),
      passwordConfirmation: Joi.any().equal(Joi.ref('password')).required(),
    };

    const {
      error
    } = Joi.validate(req.body, schema);
    if (error) return res.status(400).send(jsonError(400, false, error.details[0].message));
    else next();
  },

}

module.exports = verifyMonitorInputs;