const winston = require('winston');
const jsonError = require('./jsonError');

module.exports = function(err, req, res, next){ 
  winston.error(err.message, err); 

  res.status(500).send(jsonError(500, false, `Something failed. Ensure all requests are correct.`)); //${err}
};