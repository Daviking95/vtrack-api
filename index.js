const winston = require('winston');
const express = require('express');
const config = require('config');
require('dotenv').config();

const app = express();

require('./startup/cores');
require('./startup/logging');
require('./startup/routes')(app);
require('./startup/db')();
require('./startup/config')();
require('./startup/validation')();

// const hostname = config.get('hostname') || 'localhost';
// const port = process.env.PORT || 5000; // config.get('port') || 5000;
// const connectionUrl = config.get('dbUrl');

// To push to cloud use this command : gcloud app deploy vtrack.yaml

// To view gcloud logs : gcloud app logs read

// get the host and port name
const hostname = process.env.HOSTNAME || 'localhost';
const port = process.env.PORT || 4000;

// const connectionUrl = process.env.NODE_ENV === 'production'
//   ? process.env.MONGO_PROD_URL
//   : process.env.MONGO_LOCAL_URL;

let connectionUrl;
// Dont forget to change to production once done.
if (process.env.NODE_ENV === 'production') connectionUrl = process.env.MONGO_PROD_LIVE_URL;
else if (process.env.NODE_ENV === 'production-staging') connectionUrl = process.env.MONGO_STAGING_URL;
else if (process.env.NODE_ENV === 'local-development') connectionUrl = process.env.MONGO_LOCAL_LIVE_URL;
else connectionUrl = process.env.MONGO_STAGING_URL;

app.listen(port, () => winston.info(`App is listening on ${hostname}: ${port}, with connection url ${connectionUrl} in ${process.env.NODE_ENV} stage`));